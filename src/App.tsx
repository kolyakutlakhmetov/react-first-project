import React from 'react';
import "./styles/App.css"
import {Container} from "@mui/material";
import {BrowserRouter} from "react-router-dom";
import AppRouters from "./components/AppRouters";

function App() {

    return (
        <BrowserRouter>
            <Container>
                <AppRouters/>
            </Container>
        </BrowserRouter>
    );
}

export default App;
