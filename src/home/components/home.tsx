import React from 'react';
import {Typography} from "@mui/material";

const Home = () => {
    return (
        <Typography variant="h2" component="div" align="center">
            Домашняя страница
        </Typography>
    );
};

export default Home;