import {useMemo} from "react";
import {IPostItem} from "../types";

export const useCountPages = (getPostsFilter: IPostItem[], limit: number = 7) => {
    const count = getPostsFilter.length
    return useMemo(() => {
        if (count <= limit) return 1
        else if (count % limit === 0) return count / limit
        return ~~(count / limit) + 1
    }, [getPostsFilter])
}