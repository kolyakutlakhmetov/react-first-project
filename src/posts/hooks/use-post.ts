import {useMemo} from "react";
import {IPostItem} from "../types";

export const useFilterPost = (posts: IPostItem[], search: string, page: number, limit: number = 7): any => {
    return useMemo(() => {
        return {
            getPostsFilter: posts.filter(post => post.title.toLocaleLowerCase().includes(search.toLowerCase())),
            getFilterPage: posts
                .filter(post => post.title.toLocaleLowerCase().includes(search.toLowerCase()))
                .slice((page - 1) * limit, ((page - 1) * limit)  + limit)
        }
    }, [search, posts, page])
}