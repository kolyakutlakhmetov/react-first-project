import {IPostItem} from "../types";
import axios from "axios";

export class PostService {
    constructor() {}

    addPost(post: IPostItem): IPostItem {
        return {
            id: Date.now(),
            title: post.title,
            body: post.body,
        }
    }

    async getPosts(): Promise<IPostItem[] | []> {
        const response = await axios.get('https://jsonplaceholder.typicode.com/posts')
        if (response.status === 200) {
            return response.data
        } else {
            return []
        }
    }
}