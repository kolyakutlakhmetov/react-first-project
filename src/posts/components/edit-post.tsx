import React, {ChangeEvent, useState} from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import {IconButton} from "@mui/material";
import ModeIcon from '@mui/icons-material/Mode';
import {IPostItem} from "../types";
import PostForm from "./post-form";

interface IProps {
    post: IPostItem;
    editPost: (post: IPostItem) => void;
}

const EditPost: React.FC<IProps> = ({post, editPost}) => {
    const [open, setOpen] = useState(false);
    const [postItem, setPostItem] = useState(post)


    const handleClickOpen = () => {
        setPostItem(post)
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const save = () => {
        if (postItem.title && postItem.body) {
            editPost(postItem)
            handleClose()
        } else {
            alert('Не все поля заполнены корректно')
        }
    }

    const handleInputChange = (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        const {name, value} = event.target;
        setPostItem({...postItem, [name]: value});
    };

    return (
        <div>
            <IconButton aria-label="Редактировать" size="small" onClick={handleClickOpen}>
                <ModeIcon color="warning"/>
            </IconButton>
            <Dialog open={open} onClose={handleClose}>
                <form>
                    <DialogTitle>Редактировать пост</DialogTitle>
                    <DialogContent>
                        <PostForm postItem={postItem} handleInputChange={handleInputChange}/>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={save} color="primary">Сохранить</Button>
                        <Button onClick={handleClose} color="error">Отмена</Button>
                    </DialogActions>
                </form>
            </Dialog>
        </div>
    );
};

export default EditPost;