import React, {ChangeEvent} from 'react';
import {TextField} from "@mui/material";
import {IPostItem} from "../types";

interface IProps {
    postItem: IPostItem
    handleInputChange: (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => void
}
const PostForm: React.FC<IProps> = ({postItem, handleInputChange}) => {
    return (
        <div>
            <TextField
                required
                value={postItem.title}
                name="title"
                fullWidth
                margin="dense"
                label="Заголовок"
                size="small"
                onChange={handleInputChange}
            />
            <TextField
                required
                value={postItem.body}
                name="body"
                fullWidth
                margin="dense"
                label="Описание"
                size="small"
                onChange={handleInputChange}
                multiline
                rows={4}
            />
        </div>
    );
};

export default PostForm;