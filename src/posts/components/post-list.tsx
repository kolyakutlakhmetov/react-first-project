import React from 'react';
import {IPostItem} from '../types';
import PostItem from "./post-item";
import {Card, CardHeader, Typography} from "@mui/material";

interface IProps {
    posts: IPostItem[];
    title: string;
    deletePost: (post: IPostItem) => void;
    editPost: (post: IPostItem) => void;
}

const PostList: React.FC<IProps> = ({posts, title, deletePost, editPost}) => {

    return (
        <Card sx={{px: 1, py: 1}} variant="outlined">
            <CardHeader title={
                <Typography variant="h5" component="div" align="center">{posts.length ? title : "Посты отсутвуют"}</Typography>
            }/>
            {posts.map((post) =>
                <PostItem post={post} key={post.id} deletePost={deletePost} editPost={editPost}/>
            )}
        </Card>
    );
};

export default PostList;