import React, {ChangeEvent, useEffect, useState} from 'react';
import PostList from "./post-list";
import AddPost from "./add-post";
import {IPostItem} from "../types";
import {useFilterPost} from "../hooks/use-post";
import PostService from "../services";
import {TextField, Grid, Pagination, Button} from "@mui/material";
import {useCountPages} from "../hooks/use-count-pages";
import {Link} from "react-router-dom";


const Posts = () => {
    const title = "Список постов"

    const [posts, setPosts] = useState<IPostItem[] | []>([])
    const [search, setSearch] = useState<string>('')
    const [page, setPage] = useState<number>(1)

    useEffect(() => {
        fetchPosts()
    }, [])

    const fetchPosts = async () => {
        const response = await PostService.getPosts()
        if (response.length) {
            setPosts(response)
        }
    }

    const addPost = (newPost: IPostItem) => {
        setPosts([newPost, ...posts])
    }

    const editPost = (post: IPostItem) => {
        setPosts(posts.map(p => (p.id === post.id ? post : p)))
    }

    const deletePost = (post: IPostItem) => {
        setPosts(posts.filter(p => post.id !== p.id))
    }

    const searchPosts = ((e: ChangeEvent<HTMLInputElement>) => {
        setPage(1)
        setSearch(e.target.value)
    })

    const {getPostsFilter, getFilterPage} = useFilterPost(posts, search, page)

    return (
        <Grid container spacing={2}>
            <Grid item md={4} sx={{my: 1}}>
                <AddPost addPost={addPost}/>
                <div style={{display: "flex", justifyContent:"center", marginTop: "10px"}}>
                    <Button component={Link} to="/" variant="outlined">Домашняя страница</Button>
                </div>
            </Grid>
            <Grid item md={8} sx={{my: 1}}>
                <TextField
                    label="Поиск по заголовку..."
                    fullWidth
                    size="small"
                    value={search}
                    onChange={searchPosts}
                    sx={{mb: 1}}
                />
                <PostList posts={getFilterPage} title={title} deletePost={deletePost} editPost={editPost}/>
                <Pagination
                    page={page}
                    sx={{my: 2, display: "flex", justifyContent: "center"}}
                    count={useCountPages(getPostsFilter)}
                    variant="outlined"
                    shape="rounded"
                    onChange={(e, p) => setPage(p)}
                />
            </Grid>
        </Grid>
    );
};

export default Posts;