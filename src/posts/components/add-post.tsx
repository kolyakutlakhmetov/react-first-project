import React, {ChangeEvent, useState} from 'react';
import PostService from "../services";
import {IPostItem} from "../types";
import {Card, CardContent, Button} from "@mui/material";
import PostForm from "./post-form";

interface IProps {
    addPost: (newPost: IPostItem) => void;
}

const AddPost: React.FC<IProps> = ({addPost}) => {
    const [postItem, setPostItem] = useState({
        title: '',
        body: ''
    })

    const addNewPost = () => {
        if (postItem.title && postItem.body) {
            const newPost = PostService.addPost(postItem)
            addPost(newPost)
            setPostItem({
                title: '',
                body: ''
            })
        } else {
            alert('Не все поля заполнены корректно')
        }
    }

    const handleInputChange = (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        const { name, value } = event.target;
        setPostItem({ ...postItem, [name]: value });
    };

    return (
        <Card variant="outlined">
            <CardContent>
                <form>
                    <PostForm postItem={postItem} handleInputChange={handleInputChange}/>
                    <div style={{display: "flex", justifyContent: "center"}}>
                        <Button variant="outlined" onClick={addNewPost}>Создать</Button>
                    </div>
                </form>
            </CardContent>
        </Card>
    );
};

export default AddPost;