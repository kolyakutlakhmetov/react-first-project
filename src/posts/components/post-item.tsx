import React from 'react';
import {IPostItem} from "../types";
import {Card, CardContent, Grid, IconButton, Typography} from "@mui/material";
import CloseIcon from '@mui/icons-material/Close';
import EditPost from "./edit-post";

interface IProps {
    post: IPostItem;
    deletePost: (post: IPostItem) => void;
    editPost: (post: IPostItem) => void;
}

const PostItem: React.FC<IProps> = ({post, deletePost, editPost}) => {

    return (
        <Card variant="outlined" sx={{mt: 1}}>
            <CardContent>
                <Grid container spacing={1} columns={16}>
                    <Grid item xs={12} sm={14}>
                        <Typography variant="subtitle2" component="div" color="green">
                            {post.title}
                        </Typography>
                        <Typography variant="body2" component="div">
                            {post.body}
                        </Typography>
                    </Grid>
                    <Grid item xs={2} sm={1}>
                        <EditPost post={post} editPost={editPost}/>
                    </Grid>
                    <Grid item xs={2} sm={1}>
                        <IconButton aria-label="Удалить" size="small" onClick={() => deletePost(post)}>
                            <CloseIcon color="error"/>
                        </IconButton>
                    </Grid>
                </Grid>
            </CardContent>
        </Card>
    );
};

export default PostItem;