import React from 'react';
import {Link, Route, Switch} from "react-router-dom";
import Posts from "../posts/components/posts";
import Home from "../home/components/home";
import {Button} from "@mui/material";

const AppRouters = () => {
    return (
        <Switch>
            <Route path="/posts">
                <Posts/>
            </Route>
            <Route path="/">
                <Home/>
                <div style={{display: "flex", justifyContent:"center", marginTop: "25px"}}>
                    <Button component={Link} to="/posts" variant="outlined">Перейти к списку постов</Button>
                </div>
            </Route>
        </Switch>
    );
};

export default AppRouters;